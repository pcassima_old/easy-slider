# Easy Slider

A simple camera slider, made from easily found parts and electronics! Some 3D-printing may be required...

With the rise of 3D printers, replacement parts have become easily available and relatively cheap. As such these from the perfect basis for a cheap yet reliable camera slider.

I started this project because I wanted a motorized camera slider, but all the commercially available options where far to expensive. I had some parts laying around from an old 3D printer and used these to make a first proof-of-concept (without a motor). I used the steel guide rods and matching bearigns, the rest of the parts I 3D printed.

## Planned features

- Adding a stepper motor for motorized control (the whole point of the project)
- Buttons for local control (clicky clicky)
- OLED screen (because blindly pushing buttons isn't really productive...)
- Battery for remote operation (thinking about the Sony NP-F750's)
- Bluetooth & App control (because almost every other slider has this too & and it seems cool to implement)
